import os

# get all filex ext=md in path=root
def traverse_files(directory):
    markdown_files = []

    for root, dirs, files in os.walk(directory):
        for file in files:
			markdown_files.append(os.path.join(root, file))

    return markdown_files

# get path
def get_include_path(line):
    return line.replace("include:", "").strip()	
	

# analize file line	
def read_and_analyze_file(file_path):
    lines = []  # Массив для хранения строк файла
    with open(file_path, 'r', encoding='utf-8' ) as file:
        for line in file:
            # Анализируем каждую строку (здесь может быть ваш код анализа)
            if 'include:' in line:  # Пример: ищем строку с ключевым словом 'include'
                # Пример: Заменяем строку на другую перед добавлением её в массив
                newPath=get_include_path(line)
                print("newPath")
                print(newPath)
                lineArr = read_and_analyze_file(newPath)  # Заменяем 'include' на 'replacement'
                for list_item in lineArr:
                    lines.append(list_item)
                continue
                #print(f"Найдена и заменена строка в файле {file_path}: {line.strip()}")  # Выводим измененную строку
                #print(f"Найдена и заменена строка в файле {file_path}: {line.strip()}")  # Выводим измененную строку
            # Добавляем каждую строку в массив
            #lines.append(line.strip())  # Удаляем символы новой строки и добавляем строку в массив
            lines.append(line)  # Удаляем символы новой строки и добавляем строку в массив
    return lines	
	
	
# save dist
def save_file(lines, file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(file_path, 'w', encoding='utf-8') as file:
        for line in lines:
            #file.write(line + '\n')
            file.write(line)

# Пример использования:
directory = './src'
found_markdown_files = traverse_files(directory)

print('START ---------------------------------------')
# Выводим найденные файлы
for file_path in found_markdown_files:
    print('read file')
    print(file_path)
    print(type(file_path))
    newText=read_and_analyze_file(file_path)
    print(newText)
    print('save dist')
    file_path_dist = file_path.replace('src', 'dist')
    save_file(newText, file_path_dist)
    print('------------------')