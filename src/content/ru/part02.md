## part02 :: Работы с MySql

[Настройка MySql в рекомендациях на сайте](https://snipe-it.readme.io/docs/creating-a-database-and-user)

1. Подключение к MySql

после выполнения кода нужно написать пароль

```bash
mysql -u root -p
```

2. Создание базы данных snipeit

```bash
CREATE DATABASE snipeit;
```

3. Проверить что база созданна

```bash
SHOW DATABASES;
```

4. Создание пользователя

```bash
CREATE USER 'snipe_user'@'localhost' IDENTIFIED BY 'pass12345';
```

5. Добавление этому пользователю привелегий

```bash
grant all on snipeit.* to 'snipe_user'@'localhost';
```

> На сайте при создании пользователя есть код

```bash
CREATE USER 'newuser'@'localhost'
```

но он создает user с пустым паролем

итоги:

- созданна база данных для сайта
- созданна пользователь
- пользователю даны необходимые привелегии

оформлено в таком виде так как эти настройки можно будет вставить в конфигурационный файл **.env**

```bash
DB_DATABASE=snipeit
DB_USERNAME=snipe_user
DB_PASSWORD=pass12345
```
