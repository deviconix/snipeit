## part08 :: Доустановка и включение необходимых библиотек. Редактировнаие php.ini

[Все требования](https://snipe-it.readme.io/docs/requirements)

<details> 
  <summary>Посмотреть все требования</summary>
  
include:./src/content/ru/part08_table01.md

</details>

В моем случае не был активирован GD library

c:\tools\php\php.8.2.5\php.ini раскоментировать

```
...
;;;;;;;;;;;;;;;;;;;;;;
; Dynamic Extensions ;
;;;;;;;;;;;;;;;;;;;;;;
extension=curl
...
extension=fileinfo
...
extension=mbstring
...
extension=mysqli
...
extension=openssl
...
extension=pdo_mysql
...
extension=sodium
...
extension=zip

; 05.02.2024 snipe-it(laravel)
extension=php_gd

```
