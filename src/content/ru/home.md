# Snipe it 6.3.0

## Инструкция установки с официального сайта

[Installation](https://snipe-it.readme.io/docs/installation)

На сайте процесс установки описан очень хорошо но для меня некоторые вещи не работали.

> Команды MySql в другом синтаксисе

> При обновлении Composer нужен аккаут GitHub (при обновлении требуется токен)

### Конфигурация текущего сервера

<details>

  <summary>подробно...</summary>

include:./src/content/ru/table_config.md

</details>

## Установка на практике

include:./src/menu/ru/home/root_home_menu.md
