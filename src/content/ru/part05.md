## part05 :: Обновление Composer

при выполнении команды

```bash
composer install
```

получил ошибку (нужно обновить composer)

выполнил команду

```bash
composer update
```

получил ошибку

```
C:\var\www\snipe.it>composer update

ConEmu blocks ANSICON injection
Loading composer repositories with package information
Updating dependencies
Your requirements could not be resolved to an installable set of packages.

  Problem 1
    - Root composer.json requires php >=7.4.3 <8.2 but your php version (8.2.5) does not satisfy that requirement.
```

в файле composer.json поменял php на 8.2.10 у меня 8.2.5

```json
"require": {
    "php": ">=7.4.3 <8.2.10",
    "ext-curl": "*",
    "ext-fileinfo": "*"
	}
```

и опять выполнил команду обновления composer

```bash
composer update
```

появилась новая ошибка

```
C:\var\www\snipe.it>composer update
ConEmu blocks ANSICON injection
Loading composer repositories with package information
GitHub API limit (60 calls/hr) is exhausted, could not fetch https://api.github.com/repos/grokability/laravel-scim-server. Create a GitHub OAuth token to go over the API rate limit. You can also wait until 2024-02-05 15:05:52 for the rate limit to reset.

When working with _public_ GitHub repositories only, head to https://github.com/settings/tokens/new?scopes=&description=Composer+on+WIN-O3GTONK2JIR+2024-02-05+1412 to retrieve a token.
This token will have read-only permission for public information only.
When you need to access _private_ GitHub repositories as well, go to https://github.com/settings/tokens/new?scopes=repo&description=Composer+on+WIN-O3GTONK2JIR+2024-02-05+1412
Note that such tokens have broad read/write permissions on your behalf, even if not needed by Composer.
Tokens will be stored in plain text in "C:/Users/Yura/AppData/Roaming/Composer/auth.json" for future use by Composer.
For additional information, check https://getcomposer.org/doc/articles/authentication-for-private-packages.md#github-oauth
Token (hidden):
```

перешел по ссылке

https://github.com/settings/tokens/new?scopes=&description=Composer+on+WIN-O3GTONK2JIR+2024-02-05+1412

ранее был зарегестрирован на github дополнительно ничего не выбирал

нажал получить токен

вставил токен в файл "C:/Users/Лена/AppData/Roaming/Composer/auth.json"

auth.json
[синтаксис github-oauth:](https://getcomposer.org/doc/articles/authentication-for-private-packages.md#github-oauth)

```json
{
  "bitbucket-oauth": {},
  "github-oauth": {
    "github.com": "abc123def456ghi7890jkl987mno654pqr321stu"
  },
  "gitlab-oauth": {},
  "gitlab-token": {},
  "http-basic": {},
  "bearer": {}
}
```

пробую

> composer update

начало обновлятся

```
...
  - Installing vimeo/psalm (5.21.1): Extracting archive
  - Installing watson/validating (6.1.1): Extracting archive
 101/241 [===========>----------------]  41%    Skipped installation of bin bin/paratest.bat for package brianium/paratest: name conflicts with an existing file
100 package suggestions were added by new dependencies, use `composer suggest` to see details.
Package doctrine/reflection is abandoned, you should avoid using it. Use roave/better-reflection instead.
Package fruitcake/laravel-cors is abandoned, you should avoid using it. No replacement was suggested.
Package laravelcollective/html is abandoned, you should avoid using it. Use spatie/laravel-html instead.
Package swiftmailer/swiftmailer is abandoned, you should avoid using it. Use symfony/mailer instead.
Package nunomaduro/larastan is abandoned, you should avoid using it. Use larastan/larastan instead.
Package phpunit/php-token-stream is abandoned, you should avoid using it. No replacement was suggested.
Generating optimized autoload files
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi

In Request.php line 337:

  Symfony\Component\HttpFoundation\Request::create(): Argument #1 ($uri) must be of type string, null given, called in C:\var\www\snipe.it\vendor\laravel\framework\sr
  c\Illuminate\Foundation\Bootstrap\SetRequestForConsole.php on line 31


Script @php artisan package:discover --ansi handling the post-autoload-dump event returned with error code 1
```

выполнил команду

```bash
php artisan package:discover --ansi
```

```
...
Package manifest generated successfully.
```

обновление завершено удачно
