| название | версия                                                    | команда                                                        |
| -------- | --------------------------------------------------------- | -------------------------------------------------------------- |
| snipe-it | 6.3.0                                                     | https://github.com/snipe/snipe-it/archive/refs/tags/v6.3.0.zip |
| system   | windows 7 64                                              | >ver                                                           |
| bash     | ConEmu64                                                  | https://conemu.github.io/                                      |
| nginx    | 1.23.4                                                    | >nginx -v                                                      |
| mysql    | 8.0.33 for Win64 on x86_64 (MySQL Community Server - GPL) | >mysql -V                                                      |
| php      | PHP 8.2.5                                                 | >php -v                                                        |
| composer | 2.6.6                                                     | >composer -V                                                   |
