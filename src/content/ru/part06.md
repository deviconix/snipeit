## part06 :: Запуск установки зависимостей composer intsall

запустить установку зависимостей

```bash
composer install
```

```
C:\var\www\snipe.it>composer install
ConEmu blocks ANSICON injection
Installing dependencies from lock file (including require-dev)
Verifying lock file contents can be installed on current platform.

Nothing to install, update or remove
Package doctrine/reflection is abandoned, you should avoid using it. Use roave/better-reflection instead.
Package fruitcake/laravel-cors is abandoned, you should avoid using it. No replacement was suggested.
Package laravelcollective/html is abandoned, you should avoid using it. Use spatie/laravel-html instead.
Package swiftmailer/swiftmailer is abandoned, you should avoid using it. Use symfony/mailer instead.
Package nunomaduro/larastan is abandoned, you should avoid using it. Use larastan/larastan instead.
Package phpunit/php-token-stream is abandoned, you should avoid using it. No replacement was suggested.

Generating optimized autoload files
> Illuminate\Foundation\ComposerScripts::postAutoloadDump
> @php artisan package:discover --ansi
Discovered Package: arietimmerman/laravel-scim-server
Discovered Package: barryvdh/laravel-debugbar
Discovered Package: barryvdh/laravel-dompdf
Discovered Package: eduardokum/laravel-mail-auto-embed
Discovered Package: facade/ignition
Discovered Package: fideloper/proxy
Discovered Package: fruitcake/laravel-cors
Discovered Package: intervention/image
Discovered Package: laravel-notification-channels/microsoft-teams
Discovered Package: laravel/passport
Discovered Package: laravel/slack-notification-channel
Discovered Package: laravel/socialite
Discovered Package: laravel/tinker
Discovered Package: laravel/ui
Discovered Package: laravelcollective/html
Discovered Package: livewire/livewire
Discovered Package: maatwebsite/excel
Discovered Package: mediconesystems/livewire-datatables
Discovered Package: nesbot/carbon
Discovered Package: nunomaduro/collision
Discovered Package: nunomaduro/phpinsights
Discovered Package: pragmarx/google2fa-laravel
Discovered Package: spatie/laravel-backup
Discovered Package: unicodeveloper/laravel-password
Package manifest generated successfully.

> @php artisan vendor:publish --force --tag=livewire:assets --ansi
Copied Directory [\vendor\livewire\livewire\dist] To [\public\vendor\livewire]
Publishing complete.
141 packages you are using are looking for funding.

Use the `composer fund` command to find out more!
PHP CodeSniffer Config installed_paths set to ../../slevomat/coding-standard
```

можно пробовать запускать в браузере сайт http://snipe.it
