## part04 :: Редактирование .env

в файле .env нужно отредактировать разделы BASIC APP SETTINGS и DATABASE SETTINGS

```
# --------------------------------------------
# REQUIRED: DATABASE SETTINGS
# --------------------------------------------
...
DB_DATABASE=snipeit
DB_USERNAME=snipe_user
DB_PASSWORD=pass12345
...

# --------------------------------------------
# REQUIRED: BASIC APP SETTINGS
# --------------------------------------------
...
APP_URL= http://snipe.it

```
