## part01 :: Подготовка и тестирование хостинга

Подготовить окружение (настроить хост)

1. Создать папку **snipe.it** на сервере

```bash
mkdir c:\var\www\snipe.it\ && cd c:\var\www\snipe.it\
```

2. Создать локальный DNS

отредактировать файл **C:\Windows\System32\drivers\etc\hosts** добавив адрес сайта

```json
...
127.0.0.1 snipe.it
...
```

3. Создать конфигурацию сайта для nginx

копировать данные из оригинальной конфигурации и вставить в файл **snipe.it.conf**

[conf nginx original](https://snipe-it.readme.io/docs/linuxosx)

<details>
	<summary>посмотреть оригинал</summary>

**snipe.it.conf**

```js
server {
    listen 80;
    server_name localhost;

    root /Users/youruser/Sites/snipe-it/public/;
    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files $uri $uri/ =404;
        fastcgi_pass unix:/var/run/php5-fpm-www.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```

</details>

Создать файл **snipe.it.conf**

```bash
touch c:\tools\config_server\snipe.it.conf
```

**snipe.it.conf**

```json
server {
    listen 80;
    server_name snipe.it;

    root c:/var/www/snipe.it/public;
    index  index.php index.html index.htm;

    access_log /var/log/nginx/ams.teknex.com.au.access.log;
    error_log /var/log/nginx/ams.teknex.com.au.error.log;

    location / {
		try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        fastcgi_pass 127.0.0.1:9000;
		include fastcgi_params;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		fastcgi_intercept_errors on;
    }

}
```

подключить этот конфигурационный файл в файле **C:\tools\nginx\conf\nginx.conf**

**nginx.conf**

```

http {
include mime.types;
default_type application/octet-stream;

    access_log c:/var/log/nginx-access.log combined;
    sendfile on;

    keepalive_timeout 65;

    include c:/tools/config_server/snipe.it.conf;

    #include c:/tools/config_server/localhost.conf;
    #include c:/tools/config_server/phpmyadmin.conf;

}

```

4. Проверить конфигурацию на валидность

```bash
nginx -t
```

5. Перезапуск сервера

```bash
nginx -s reload
```

6. Создать файл **index_test.php**

```bash
printf "<?php\n\tphpinfo();\n?>" > "c:/var/www/snipe.it/public/index_test.php"
```

7. Проверить конфигурацию. В браузере перейти по ссылке

   http://snipe.it/index_test.php

   должна вывестись информация о PHP

   ![img phpinfo](../../img/phpinfo.png)

итоги:

- созданно место на сервере для сайта
- создан DNS для сайта
- создана конфигурация nginx для сайта
- создан файл с выводом информации о php
- протестированы на работоспособность настройки конфигурация сервера

\>\>**part01 :: Подготовка и тестирование хостинга**
