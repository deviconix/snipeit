## part03 :: Скачивание, распаковка дистрибутива и файла конфигурации. Генерация ключа

1. скачать дистрибутив [snipe-it-6.3.0.zip](https://github.com/snipe/snipe-it/archive/refs/tags/v6.3.0.zip)

2. Разархивировать в папке с сайтом

```bash
c:\var\www\snipe.it>unzip snipe-it-6.3.0
```

4. перенести на уровень выше (в корень проекта)

```bash
c:\var\www\snipe.it>mv snipe-it-6.3.0/{*,.*} .
```

5. удалить не нужную папку распакованную после архива snipe-it-6.3.0

```bash
c:\var\www\snipe.it>rd /s /q snipe-it-6.3.0
```

6. создать файл .env из .env.example

```bash
cp c:\var\www\snipe.it\.env.example .env
```

7. создать ключ приложения

```bash
cd c:\var\www\snipe.it\ & php artisan key:generate
```

итоги:

- скачаный дистрибутив распакованный на сервере
- создан ключ приложения
