## part07 :: Открытие страницы в браузере

после перехода на сайт

```bash
http://snipe.it
```

был редирект на

```bash
http://snipe.it/setup
```

step 1

Check configuration

![dddd](../../img/step01.jpg)

<details> 
  <summary>если ошибка 500</summary>

нужно сгенерировать ключ приложения

```
C:\var\www\snipe.it>php artisan key:generate
ConEmu blocks ANSICON injection
**************************************
*     Application In Production!     *
**************************************

 Do you really wish to run this command? (yes/no) [no]:
 > y

Application key set successfully.
```

в файл .env добавился ключ

```json
APP_KEY=base64:OL6suEMGgVoRw5JpQy6NJKJxnIWPj2+iiWk9iIH9H+o=
```

обновляем страницу и запускается setup

</details>

идет проверка необходимых параметров

> У меня не все пункты были зеленые поэтому присутствует пункт 8
