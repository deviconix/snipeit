import mistune

# 13.02.2024
def convert_md_to_html(file_path):
    with open(file_path, 'r', encoding='utf-8' ) as file:
        text = file.read()
        #html = markdown.markdown(text,  extensions=['fenced_code','tables', 'codehilite'])
        # escape=False не нужно экранировать HTML символы
        # html = mistune.markdown(text, escape=False)
        #--markdown = mistune.create_markdown(escape=False)
        # or markdown = mistune.Markdown(...)
        markdown = mistune.create_markdown(plugins=['table'],escape=False)

        html = markdown(text)
    return html