from helpers.file_utils import save_html
from helpers.md_utils import convert_md_to_html

def scenario_convert(arr_path, template_html):
    print('convert Start')
    print(arr_path)
    for file_path in arr_path:
        print('read file')
        print(file_path)
        html_content = convert_md_to_html(file_path)
        print('html')

        # в href=*.md => href=*.html
        # replace link
        html_content=html_content.replace('.md', '.html')
        # use template
        html_page = template_html.replace('{{content}}', html_content)

        print(html_page)
        file_path_html = file_path.replace('dist', 'html')
        file_path_html = file_path_html.replace('.md', '.html')
        print(file_path_html)
        save_html(file_path_html, html_page)
        print('------------------')