import os

# open txt file
def open_file(file_path):
    with open(file_path, 'r', encoding='utf-8' ) as file:
        text = file.read()
    return text

def save_html(file_path, html):
    # check directories
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(html)
    print('save_html()')
    print(directory)

# get all files ext=md in path=root
def traverse_files_deep(directory):
    markdown_files = []

    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.md'):
                markdown_files.append(os.path.join(root, file))

    return markdown_files

# get all files in root
def traverse_files(directory):
    markdown_files = []

    for file in os.listdir(directory):
        file_path = os.path.join(directory, file)
        if os.path.isfile(file_path) and file.endswith('.md'):
            markdown_files.append(file_path)
            
    return markdown_files