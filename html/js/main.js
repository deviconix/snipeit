window.onload = init

function init() {
	console.log('start')
	// get the list of all highlight code blocks
	//const highlights = document.querySelectorAll("div.highlight");
	const highlights = document.querySelectorAll("pre .language-bash");
	//console.log(highlights)
	highlights.forEach((div) => {
		// Получаем ссылку на элемент в который мы хотим добавить новый элемент
		let parentElement = div.closest('pre')
		//console.log(parentElement)
		// Получаем ссылку на первый дочерний элемент
		let theFirstChild = parentElement.firstChild;




		// create the copy button
		// Создаём новый элемент, который будем добавлять
		const newElement = document.createElement("button");
		newElement.innerHTML = "Copy";
		// add the event listener to each click
		newElement.addEventListener("click", handleCopyClick);
		// append the copy button to each code block
		//div.append(copy);

		// Вставляем новый элемент перед первым дочерним элементом
		parentElement.insertBefore(newElement, theFirstChild);
	});

}


const copyToClipboard = str => {
	const el = document.createElement("textarea") // Create a <textarea> element
	el.value = str // Set its value to the string that you want copied
	el.setAttribute("readonly", "") // Make it readonly to be tamper-proof
	el.style.position = "absolute"
	el.style.left = "-9999px" // Move outside the screen to make it invisible
	document.body.appendChild(el) // Append the <textarea> element to the HTML document
	const selected =
		document.getSelection().rangeCount > 0 // Check if there is any content selected previously
			? document.getSelection().getRangeAt(0) // Store selection if found
			: false // Mark as false to know no selection existed before
	el.select() // Select the <textarea> content
	document.execCommand("copy") // Copy - only works as a result of a user action (e.g. click events)
	document.body.removeChild(el) // Remove the <textarea> element
	if (selected) {
		// If a selection existed before copying
		document.getSelection().removeAllRanges() // Unselect everything on the HTML document
		document.getSelection().addRange(selected) // Restore the original selection
	}
}


function handleCopyClick(evt) {
	//
	let newdocument = evt.target.parentElement
	const code = newdocument.querySelector(".language-bash");
	const { innerText } = code
	const innerText2 = innerText.replace(/\r?\n/g, "")
	copyToClipboard(innerText2)
}